import datetime
import glob
import os
import time
from shutil import move

import openpyxl
import pandas as pd
from openpyxl import load_workbook
from selenium import webdriver

"""
This script grabs the logs from the carel micro controller and will store them on the server.
The save path needs to be changed for that though. This also works for a bunch of different browsers
but I don't use Chrome because it is garbage so I figured I'd give it a single job to do.
"""
# Current date/time
date = datetime.date.today()

# Need to download the browser driver online @ https://sites.google.com/a/chromium.org/chromedriver/downloads
exe_path = "C:\Program Files (x86)\chromedriver.exe"

# This changes the default directory that the file is saved to
prefs = {"download.default_directory": r"X:\Chiller Capacity Testing"}
# prefs = {
#    "download.default_directory": r"C:\Users\Hank\Documents\Random Python Scripts\selenium_web_scrape"
# }
options = (
    webdriver.ChromeOptions()
)  # Changed chrome_options to options because chrome_options is depreciated.

# chrome_options.add_argument("--headless")#makes it so that the browser doesn't actually open
options.headless = True  # this also makes it work without opening the browser

options.add_argument("--log-level=3")  # This should get rid of a depreciation error.

options.add_experimental_option("prefs", prefs)
# applies the above changes to the webdriver
driver = webdriver.Chrome(exe_path, options=options)
try:
    print("Started")
    # website address
    driver.get(
        "http://192.168.1.101/logger.htm"
    )  # Just using this as a test to see if it's working.
    # driver.get("http://192.168.1.187/logger.htm")

    # find_element_by_id uses the html code from the website.
    # To find this, right click on the object and hit "Inspect Element" and copy the id
    link = driver.find_element_by_id("btnQuickSavelogs")
    # .click() makes it so that the button is clicked on... Duh.
    link.click()
    # This print function is just to make sure that this is actually working.
    print("File has been downloaded")

    # This will make it so the csv's name has the current date added to it.
    save_path = r"X:\Chiller Capacity Testing"
    csv_recorded = os.path.join(save_path, "Recorded CSV's")
    # This is just to make sure the path is correct.
    print(os.path.join(save_path, "getlog.csv"))
    # This is putting it to sleep for X seconds to give the file time to finish downloading
    time.sleep(60)
    # This is renaming the file using shutil.move
    move(
        os.path.join(save_path, "getlog.csv"),
        os.path.join(save_path, f"getlog-{date}.csv"),
    )
    print("File has been renamed")
    """
    Now that everything is in the right location the CSV's need 
    to be merged into a larger file which is what the function
    below is taking care of.
    """

    """
    Opens up the excel worksheet
    """
    master_file = os.path.join(save_path, "Masterfile.xlsx")
    book = load_workbook(master_file)
    writer = pd.ExcelWriter(master_file, engine="openpyxl")
    writer.book = book
    """
    This may need to be editted because I probably don't want to merge the xlsx(Excel)
    file because it will merge the information twice. So since the file only downloads as a 
    CSV file I might just search for a file with the ending ".csv". I will take a look 
    at it tomorrow and see if this is working.
    """

    def merge():
        df = pd.DataFrame()
        files = []
        lists = os.walk(save_path)
        for path, dir, filenames in lists:
            for filename in filenames:
                if filename.endswith(".xlsx"):
                    doc = os.path.join(path, filename)
                    print(doc)
                    df = df.append(pd.read_excel(doc,engine="openpyxl"))
                    files.append(df)
                if filename.endswith(".csv"):
                    doc = os.path.join(path, filename)
                    print(doc)
                    df = df.append(pd.read_csv(doc))
                    files.append(df)
        mil = 1000000
        val = int(df.shape[0] / mil)
        merged_df = pd.concat(files, ignore_index=True)
        df_num = globals()
        for i in range(val + 1):
            df_num[f"df_{i}"] = f"df_{i}"

        for i in range(val + 1):
            if i == 1:
                df_1 = merged_df.iloc[:mil, :]
                df_1.to_excel(writer, sheet_name=f"Page {i}", index=True)

            elif i > 1:
                """
                Basically currently set up to deal with 7 million
                entries.
                """

                # df_1 = merged_df.iloc[:mil, :]
                # df_1.to_excel(writer,sheet_name=f"Page {i}", index=True)
                df_2 = merged_df.iloc[:mil, :]
                df_2.to_excel(writer, sheet_name=f"Page {i}", index=True)
                df_3 = merged_df.iloc[:mil, :]
                df_3.to_excel(writer, sheet_name=f"Page {i}", index=True)
                df_4 = merged_df.iloc[:mil, :]
                df_4.to_excel(writer, sheet_name=f"Page {i}", index=True)
                df_5 = merged_df.iloc[:mil, :]
                df_5.to_excel(writer, sheet_name=f"Page {i}", index=True)
                df_6 = merged_df.iloc[:mil, :]
                df_6.to_excel(writer, sheet_name=f"Page {i}", index=True)
                df_7 = merged_df.iloc[:mil, :]
                df_7.to_excel(writer, sheet_name=f"Page {i}", index=True)

        # merged_1 = df.iloc[:1000000, :]
        # merged_2 = df.iloc[1000001:, :]
        # merged_1.to_excel(writer, sheet_name='Page 1')

        # merged_df = pd.concat(files, ignore_index=True)
        # merged_1 = df.iloc[:1000000, :]
        # merged_2 = df.iloc[1000001:, :]
        # merged_1.to_excel(writer, sheet_name='Page 1')
        # merged_2.to_excel(writer, sheet_name='Page 2')
        # merged_df.to_excel(os.path.join(save_path, "MasterFile.xlsx"))
        print(f"Files have been merged at {save_path}")

    merge()
    writer.save()
    writer.close()
    time.sleep(120)
    # Now that everything has been merged I want to take the files
    # That have been merged and move them to a different location.
    # csv_recorded = os.path.join(save_path, "Recorded CSV's")
    move(
        os.path.join(save_path, f"getlog-{date}.csv"),
        os.path.join(csv_recorded, f"getlog-{date}.csv"),
    )


except Exception as e:
    print(e)

    # fail = "It could also be that the internet is super slow so the download is finishing after the script already runs."
    # print(f"Either the IP is down or you have changed the paths. Figure it out. {fail}")
