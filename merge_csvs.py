import os
import datetime
import time
from shutil import move
import pandas as pd
import openpyxl
from openpyxl import load_workbook

save_path = r"X:\Chiller Capacity Testing"

"""
Maybe how this needs to be done is
by only concatinating the most recent
download into the masterfile. Though 
I don't know how that would then be able
to handle the changing sheets because 
eventually they would get overfilled.
"""


#file_locations = glob.glob(os.path.join(path, "getlog*.csv"))

master_file = os.path.join(save_path, "Masterfile.xlsx")
book = load_workbook(master_file)
writer = pd.ExcelWriter(master_file, engine='openpyxl')
writer.book = book

def merge():
        df = pd.DataFrame()
        files = []
        lists = os.walk(save_path)
        for path, dir, filenames in lists:
            for filename in filenames:
                if filename.endswith(".xlsx"):
                    doc = os.path.join(path, filename)
                    print(doc)
                    df = df.append(pd.read_excel(doc,engine="openpyxl"))
                    files.append(df)
                if filename.endswith(".csv"):
                    doc = os.path.join(path, filename)
                    print(doc)
                    df = df.append(pd.read_csv(doc))
                    files.append(df)
        mil = 1000000
        val = int(df.shape[0]/mil)
        merged_df = pd.concat(files, ignore_index=True)
        print(val)
        df_num = globals()
        for i in range(val+1):
            df_num[f"df_{i}"] = f"df_{i}"

        for i in range(val+1):
            if i == 1:
                df_1 = merged_df.iloc[:mil, :]
                df_1.to_excel(writer,sheet_name=f"Page {i}", index=True)

            elif i > 1:
                """
                Basically currently set up to deal with 7 million
                entries.
                """

                df_1 = merged_df.iloc[:mil, :]
                df_1.to_excel(writer,sheet_name=f"Page {i}", index=True)
                df_2 = merged_df.iloc[:mil, :]
                df_2.to_excel(writer,sheet_name=f"Page {i}", index=True)
                df_3 = merged_df.iloc[:mil, :]
                df_3.to_excel(writer,sheet_name=f"Page {i}", index=True)
                df_4 = merged_df.iloc[:mil, :]
                df_4.to_excel(writer,sheet_name=f"Page {i}", index=True)
                df_5 = merged_df.iloc[:mil, :]
                df_5.to_excel(writer,sheet_name=f"Page {i}", index=True)
                df_6 = merged_df.iloc[:mil, :]
                df_6.to_excel(writer,sheet_name=f"Page {i}", index=True)
                df_7 = merged_df.iloc[:mil, :]
                df_7.to_excel(writer,sheet_name=f"Page {i}", index=True)

        #merged_1 = merged_df.iloc[:1000000, :]
        #print(merged_1.shape)
        #merged_2 = merged_df.iloc[1000000:2000001:, :]#So this makes it work but just having it in there doesn't work.
        #print(merged_2.shape)
        #merged_3 = merged_df.iloc[2000001:, :]
        #print(merged_3.shape)
        #merged_1.to_excel(writer, sheet_name='Page 1')
        #merged_2.to_excel(writer, sheet_name='Page 2')
        #merged_3.to_excel(writer, sheet_name='Page 3')
        """
        mil = 1000000
        for i in range(len(df.shape[0]/mil)):#Gives us how many million entries there are.
            if i == 1:
                df.iloc[:mil, :]
                merge.to_excel(writer,sheet_name=f"Page {i}")
            elif:
                df.iloc[(i-1)*mil+1:i*mil,:]
                merge.to_excel(writer,sheet_name=f"Page {i}")
        """

        #merged_df.to_excel(os.path.join(save_path, "MasterFile.xlsx"))
        print(f"Files have been merged at {save_path}")
merge()
writer.save()
writer.close()
time.sleep(120)
# Now that everything has been merged I want to take the files
# That have been merged and move them to a different location.
# csv_recorded = os.path.join(save_path, "Recorded CSV's")
#move(
#       os.path.join(save_path, f"getlog-{date}.csv"),
#       os.path.join(csv_recorded, f"getlog-{date}.csv"),
#)
        
