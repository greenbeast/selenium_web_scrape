import datetime
import glob
import os
import time
from shutil import move

import openpyxl
import pandas as pd
from openpyxl import load_workbook
from selenium import webdriver


class web_scrape:
    date = datetime.date.today()
    exe_path = "C:\Program Files (x86)\chromedriver.exe"
    prefs = {"download.default_directory": r"X:\Chiller Capacity Testing"}
    options = webdriver.ChromeOptions()
    options.headless = True
    options.add_argument("--log-level=3")
    options.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(exe_path, options=options)
    try:
        print("Started")
        driver.get("https://192.168.1.101/logger.htm")
        link = driver.find_element_by_id("btnQuickSavelogs").click()
        print("File has been downloaded")
        save_path = r"X:\Chiller Capacity Testing"
        csv_recorded = os.path.join(save_path, "Recorded CSV's")
        time.sleep(30)
        move(
            os.path.join(save_path, "getlog.csv"),
            os.path.join(save_path, f"getlog-{date}.csv"),
        )
        print("File has been renamed")
        master_file = os.path.join(save_path, "Materfile.xlsx")
        book = load_workbook(master_file)
        writer = pd.ExcelWriter(master_file, engine="openpyxl")
        writer.book = book

        @property
        def merge(self):
            df = pd.DataFrame()
            files = []
            lists = os.walk(save_path)
            for path, fir, filenames in lists:
                for filename in filenames:
                    if filename.endswith(".xlsx"):
                        doc = os.path.join(path, filename)
                        print(doc)
                        df = df.append(pd.read_excel(doc, engine="openpyxl"))
                        files.append(df)
                    if filename.endswith(".csv"):
                        doc = os.path.join(path, filename)
                        print(doc)
                        df = df.append(pd.read_csv(doc))
                        files.append(df)
            mil = 1000000
            val = int(df.shape[0] / mil)
            merged_df = pd.concat(files, ignore_index=True)
            df_num = globals()
            for i in range(val + 1):
                df_num[f"df_{i}"] = f"df_{i}"

            for i in range(val + 1):
                if i == 1:
                    df_1 = merged.iloc[:mil, :]
                    df_1.to_excel(writer, sheet_name=f"Page{i}", index=True)
                elif i > 1:
                    df_2 = merged_df.iloc[:mil, :]
                    df_2.to_excel(writer, sheet_name=f"Page {i}", index=True)
                    df_3 = merged_df.iloc[:mil, :]
                    df_3.to_excel(writer, sheet_name=f"Page {i}", index=True)
                    df_4 = merged_df.iloc[:mil, :]
                    df_4.to_excel(writer, sheet_name=f"Page {i}", index=True)
                    df_5 = merged_df.iloc[:mil, :]
                    df_5.to_excel(writer, sheet_name=f"Page {i}", index=True)
                    df_6 = merged_df.iloc[:mil, :]
                    df_6.to_excel(writer, sheet_name=f"Page {i}", index=True)
                    df_7 = merged_df.iloc[:mil, :]
                    df_7.to_excel(writer, sheet_name=f"Page {i}", index=True)
                print(f"Files have been merged at {save_path}")
            writer.save()
            writer.close()
            time.sleep(120)
            move(
                os.path.join(save_path, f"getlog-{date}.csv"),
                os.path.join(csv_recorded, f"getlog-{date}.csv"),
            )

    except Exception as e:
        print(e)


ws = web_scrape()
ws.merge
